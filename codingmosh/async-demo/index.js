console.log('before');
getUser(2, (user)=>{
    console.log(user)
    getRepo(user,(repos)=>{
        console.log(repos);

    })
})

console.log('after');

//callbacks
//promises
//async await

function getUser (id, callback){
    setTimeout(() => {
        console.log('before2');
        callback({
            id:id,
            gethubUserId:'mosh'
        })
    }, 100);
}

function getRepo(username, callback){
    setTimeout(() => {
        console.log('before2');
        callback(
            [ 'mosh 1'
            , 'mosh 2'
            , 'mosh 3'
            ]
           )
    }, 100);
}