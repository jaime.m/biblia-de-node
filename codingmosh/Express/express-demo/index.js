const config = require('config')
const express = require('express')
const morgan = require('morgan')
const Joi = require('joi')
const startupDebugger = require('debug')('app:startup')
const dbDebugger=require('debug')('app:db')
const Logger = require('./logger')
const server = express()
const helmet= require('helmet')
const courses = require('./routes/courses')
const customers  =require('./routes/customers')
const port =process.env.PORT || 3100

const mongoose = require('mongoose')
const {Schema} = mongoose

mongoose.connect('mongodb://localhost/vidly',{
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(()=>{
    console.log('connected to mongo db');
}).catch(err =>{
    console.error('error');
})

server.set('view engine','pug')
server.set('views','./views')

//configuration 
console.log('Application Name :'+ config.get('name') )
console.log('Mail server Name :'+ config.get('mail.host') )
//console.log('Mail password :'+ config.get('mail.password') )

console.log(`Node env: ${process.env.NODE_ENV}`)//undefined

if(server.get('env')==='development'){
   
    console.log('morgan enabled');
    server.use(morgan('tiny'))
    
}
//it deppends ogf the environment variables called debug=app:startup you can use a willcard * and see all the debugs
dbDebugger('Connected to database')

server.use(express.json())
server.use(express.urlencoded({extended:true}))
server.use(express.static('public'))
//the middleware will im pact the performance of the application
server.use(helmet())

//the request will be logged 
//aLL FUNCTIONS HERE ARE MIDDLEWARES 
server.use(Logger)
server.use('/api/genres',courses)
server.use('/api/genres',customers)


server.listen(port,()=>{
    console.log(`Server running in port ${port}`);
    
})

function validategenre(genre){
    const schema ={
        name:Joi.string().min(3).required()
    }
    return result= Joi.validate(genre, schema)
}