const express = require('express')
const router = express.Router()

 router.get('/',async (req,res)=>{
    return await  res.send(customers)

 })
 
 router.get('/:id',async (req,res)=>{
 let customer = await customers.findById(req.params.id)
 if (customer)
     res.send(customer)
     else
     return res.status(404).send({
         status:404,
         message:"the customer cannot be found"
     })
 })
 
 router.post('/',async (req,res)=>{
 
     const schema ={
         name:Joi.string().min(3).required()
     }
     const result= Joi.validate(req.body, schema)
     if(result.error){
         res.status(400).send(result.error.details[0].message)
         return
     }
     const customer = new customers({
         name:req.body.name,
         phone:req.body.phone,
         isGold:body.name.isGold,
     })

     customer = await  customer.save()
    res.send(customer)
 })
 router.put('/:id', async (req,res)=>{
 
 const {error} = validatecustomer(req.body)
     if(error){
         res.status(400).send(error.details[0].message)
         return
     }
 
     let customer =customers.findByIdAndUpdate(req.params.id,req.body, {new :true})
     if (customer){
        res.send(customer)
     }
     else
     return res.status(404).send({
         status:404,
         message:"the customer cannot be found"
     })
 
 })
 
 router.delete('/:id',async (req,res)=>{
     let customer =await customers.findByIdAndDelete(req.params.id)
     if (customer){
         const index = customers.indexOf(customer)
         customers.splice(index,1)
 
     res.send(customer)
     }
     else
    return res.status(404).send({
         status:404,
         message:"the customer cannot be found"
     })
 })
 
 module.exports=router