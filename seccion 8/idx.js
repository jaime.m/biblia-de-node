const http = require('http')
const fs = require('fs')

const about =fs.readFileSync('./about.html')
const html =fs.readFileSync('./index.html')

http.createServer((request,response)=>{

    const {url}=request
    if(url==='/'){
            response.writeHead(200,{"Content-Type":"text/html"})
            response.write(html)
    }else if(url==='/about'){
        response.writeHead(200,{"Content-Type":"text/html"})
        response.write(about)
    }else{
        response.writeHead(404,{"Content-Type":"text/html"})
        response.write('no existis')
    }

    response.end()
}).listen(8081)

