

//Ciclos 

const fruit =['apple','melon','lemon']

for(let i=0; i<fruit.length;i++){
    console.log(fruit[i]);
    
}

for(const fru of fruit){
    console.log(fru)
}

const people =[
    {name:"copy"},
    {name:"copy 2"}
]

for (const person in people){
    console.log(people[person].name);
}

//funciones

function greet(){
    console.log("hi");

}

const greeter = (name)=>
    console.log(`Hola ${name} `); 


greeter("copy")

//callbacks

const booksDb =[
    {
        id:1,
        title:'librazo',
        authorId:1
    },
    {
        id:2,
        title:'librazo 2',
        authorId:1
    },
    {
        id:3,
        title:'librazo 3',
        authorId:1
    },
    {
        id:4,
        title:'librazo 4',
        authorId:1
    },
]

const AuthorDb =[
    {
        id:1,
        name:'pep'
    },
    {
        id:2,
        name:'pep 2 2'
    }
]

async function getBookById(id){
        const book = booksDb.find(book=>book.id===id)
        if(!book){
            const error= new Error()
            error.message="Book not found"
            throw error
        }
        return book
   
}

async function getAuthorById(id){

        const author = AuthorDb.find(author=>author.id===id)
        if(!author){
            //siempre se le envía un error como pr nimer error a un callback
            const error= new Error()
            error.message="Book not found"
            throw (error)
        }
    return (author)

  
    

}
/*
getBookById(2)
.then(book=>{
    return getAuthorById(book.authorId)
})
.then(author=>{
    console.log(author)
    
})
.catch(error=>{
    console.log(error.message);
    
})*/
//Promises como funciona js desdepues de es6

//Async y await

async function main(){    
try{
    const book =await  getBookById(71)
    const author = await getAuthorById(book.authorId)
    console.log(`this book ${book.title} was written by ${author.name}`);
}catch(ex){
    console.log(ex.message);
    
}
    
}

main()