const {createContainer,asClass,asValue,asFunction} =require('awilix')
const container = createContainer()

//Importar los servicios
const {HomeService} =require('../services')
//importar los controllers
const {HomeController} =require('../controllers')
//Importar las rutas
const {HomeRoutes} =require('../routes/index.routes')
const Routes= require('../routes')

//importar el app
const app =require('.')

//Config
const config =require('../config')
//Configuración principal app
container.register({
    router: asFunction(Routes).singleton(),
    config:asValue(config),
    app:asClass(app).singleton
}).register({
    HomeService:asClass(HomeService).singleton()
}).register({
    HomeController:asClass(HomeController.bind(HomeController)).singleton()
}).register({
    HomeRoutes: asFunction(HomeRoutes).singleton()
})

module.exports=container