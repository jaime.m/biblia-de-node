const {Router}= require('express')



module.exports= function({HomeController}){
    const router = Router()
    //El scope que se transfiere es el de express, sin embargo por eso en la inyección de dependencias se realizó el proceso de mandar el bind para que se envíe el scope de la función

    router.get('/',HomeController.index)
    return router

}