const mongoose = require('mongoose')
const axios =require('axios').default
const {MONGO_URI}=require('./config')
const cheerio=require('cheerio')
const cron=require('node-cron')
const {BreakingNew} =require('./models')

mongoose.connect(MONGO_URI,{
    useNewUrlParser:true,
    useCreateIndex:true
})

cron.schedule("* * * * * *",async ()=>{
    console.log('Cronjob excecuted!');
    
    const html = await axios.get('https://cnnespanol.cnn.com/')
    const $ = cheerio.load(html.data)
    const title =$(".news__title")
    title.each((index,element)=>{
        const breakingNew={
            title:$(element).text().trim(),
            link:$(element).children().attr("href")
        }
       // console.log(breakingNew)
        BreakingNew.create([breakingNew])
    })
   
})


/*

const Cat =mongoose.model("Cat",{
    name:String
})

const kitty = new Cat({
    name:"mimi!"
})
kitty.save().then(console.log('Mimi has been saved')
)

Cat.find().then(console.log)*/