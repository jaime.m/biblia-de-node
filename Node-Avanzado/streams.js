const fs= require('fs')
/*
console.time('tiempo de respuesta')

for(let i=0;i <10;i++){
    fs.readFileSync("archivo.txt","utf8")
}

console.timeEnd('tiempo de respuesta')*/
/*
console.time('tiempo de respuesta')

for(let i=0;i <10;i++){
    fs.createReadStream("archivo.txt","utf8")
}

console.timeEnd('tiempo de respuesta')*/

//Ejemplo de lectura
const streamLectura =fs.createReadStream("archivo.txt",{encoding:"utf8"})

streamLectura.on("open",()=>{
    console.log("abriendo archivo");
    
}).on("data",()=>{
    console.log("----");
    
}).on("close",()=>{
    console.log(('archivo cerrado'));
    
}).on("error",()=>{
    console.log('error de archivo');
    
})

//ejemplo de escritura

var contenido="123123123123"
var iteraciones =15
for(let i=0; i<=iteraciones;i++){

    contenido+=contenido
}

fs.writeFile('./archivo2.txt',contenido,()=>{
    console.log('escritura realizada');
})

const streamEscritura =fs.createWriteStream("archivo3.txt",{encoding:"utf8"})
 
streamEscritura.write(contenido, res =>{
    console.log("stream finalizado");
    
})