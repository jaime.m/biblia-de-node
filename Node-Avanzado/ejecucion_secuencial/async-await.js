function nA(){

    return new Promise ((resolve,reject) =>{

        setTimeout(()=>{
            resolve(Math.floor(Math.random()*100))
        },2000)
        
    })
}

function lA(){

    return new Promise ((resolve,reject) =>{

        setTimeout(()=>{
            resolve(Math.floor(Math.random()*100))
        },2000)
        
    })
}
 /*async function resultado ()
 {
     console.log('resultado invocado');
     
     const  a= await nA()
     console.log('resultado ',a);
     
 }

 resultado()*/

 nA().then(()=>{
    console.log('usuario autenticado ');

    lA().then(()=>{
console.log('datos de usuario correctos');

    })
 }).catch(()=>{
     console.log('error de logueo');
     
 })
 

 function mensajesPrivados(){
    return new Promise ((resolve,reject) =>{

        setTimeout(()=>{
            resolve('mensajes privados')
        },2000)
        
    })
}


function fotos(){
    return new Promise ((resolve,reject) =>{

        setTimeout(()=>{
            resolve('fotos')
        },2000)
        
    })
}

function transacciones(){
    return new Promise ((resolve,reject) =>{
        setTimeout(()=>{
            resolve('transacciones')
        },2000)
        
    })
}

Promise.all([mensajesPrivados(),fotos(),transacciones()]).then((valores)=>{
    console.log(valores);
    
})