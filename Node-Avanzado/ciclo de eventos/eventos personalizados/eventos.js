const fs = require('fs')
const EventEmitter = require('events')

class Emisor extends EventEmitter{

}
const miEmisor = new Emisor()

let streamEscritura = fs.createWriteStream('archivo.txt',{
    encoding:'utf8'
})

function escribirArchivo(){
    var ite=5
    for(let i=0;i<ite;i++){
        streamEscritura.write(`Iteració...n ${i} \n`)
    }
    streamEscritura.write(`==================END=================`)

    streamEscritura.end()
}

function correo(){
    console.log('preparando correo');
    setTimeout(()=>{

        miEmisor.emit('correoOk')
    },1000

    )
    
}

function leerDocumento(){
    fs.readFile('./archivo.txt', (error,doc)=>{
        console.log(doc.toString());
        
    })
}

streamEscritura.on('close',()=>{
   correo()
})

miEmisor.on('correoOk',()=>{
    leerDocumento()
})
escribirArchivo()
