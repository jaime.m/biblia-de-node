const fs = require('fs')
const util = require('util')
/*
fs.writeFile('./archivos/arhivo.txt',"2342123",()=>{
    console.log('todo bien');
    
})*/

const writeFilePromesa = util.promisify(fs.writeFile)

writeFilePromesa('./archivos/arhivo.txt',"2342123").then(()=>{
    console.log('ok');
    
}).catch(()=>{
    console.log('error');
    
})