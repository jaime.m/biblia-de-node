const fs = require('fs')

const { Duplex, Transform}= require('stream')

const streamLectura =fs.createReadStream("./base.txt")
const streamEscritura=fs.createWriteStream('./destino.txt')
//streamLectura.pipe(streamEscritura)
 
/*streamLectura.on("end",()=>{
    console.log(('proceso finalizado'));
    
})*/

//stream duplex

const reporte = new Duplex({
    write(data,encode,callback){
        console.log(data);
        
        callback();
    },
    read(size){
    }
})

streamLectura.setEncoding("utf8")
const filtro = new Transform({
    writableObjectMode:true,
    transform(data, encoding,callback){
        this.push(data.toString().toUpperCase())
        callback()
    },
    final(callback){
        callback()
    }
})

streamLectura.pipe(filtro).pipe(streamEscritura)


//steam transform

