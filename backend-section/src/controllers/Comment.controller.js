let _commentService=null

class Commentcontroller{
    constructor({UserService}){
        _commentService
    =UserService
    }
    async get (req,res){
        const {commentId}=req.params
        const user = await _commentService
    .get(commentId)
        return res.send(user)
    }

    async update (req,res){
        const {body}  = req
        const {commentId}=req.params
        const updatedUser = await _commentService
    .update(commentId,body)
        return res.send(updatedUser)
    }

    async delete(req,res){
        const {commentId}=req.params

        const deletedUser = await _commentService
    .delete(commentId)
        return res.send(deletedUser)
    }

    async getIdeasComments(req,res){
        const {ideaId} = req.params
        const ideas = await  _commentService
    .getIdeasComments(ideaId)
        return res.send(ideas)
    }

    async createComment(req,res){
        const {body} = req
        
        const {ideaId} = req.params
        
        const ideas = await  _commentService
    .createdComment(body,ideaId)
        return res.status(201).send(ideas)
    }

    

}


module.exports=Commentcontroller