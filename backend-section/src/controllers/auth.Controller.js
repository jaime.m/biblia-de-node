let _authService = null

class AuthController{

    constructor({AuthService}){
        _authService=AuthService
    }
    async singup(req ,res){
        const {body} =req
  
        const createdUser= await _authService.singup(body)
        return res.status(201).send(createdUser)
    }
    async singin(req ,res){
        const {body} =req
        const creds= await _authService.singin(body)
        return res.status(200).send(creds)
    }

}

module.exports=AuthController