module.exports ={
    HomeController:require('./home.controller'),
    UserController:require('./user.controller'),
    IdeaController:require('./idea.controller'),
    CommentsController:require('./Comment.controller'),
    AuthController:require('./auth.Controller')
}