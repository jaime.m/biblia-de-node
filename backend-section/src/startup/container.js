const {createContainer, asClass,asValue,asFunction } = require('awilix')

//Config
const config=require('../config')
const app = require('.')

//Services
const {HomeService, UserService,IdeaService,CommentService,AuthService} =require('../services')

// Controllers
const {HomeController,UserController,IdeaController,CommentsController,AuthController} =require('../controllers')

//Routes
const {HomeRoutes,IdeaRoutes,CommentRoutes,UserRoutes,AuthRoutes}=require('../routes/index.routes')
const Routes = require('../routes')

//repositories
const {UserRepository, IdeaRepository,CommentRepository} =require('../repositories')


//Models

const{User,Comment,Idea} = require('../models')

const container =createContainer()

container
.register({
    router:asFunction(Routes).singleton(),
    config:asValue(config),
    app:asClass(app).singleton()
})
.register({
    HomeService:asClass(HomeService).singleton(),
    IdeaService:asClass(IdeaService).singleton(),
    AuthService:asClass(AuthService).singleton(),
    CommentService:asClass(CommentService).singleton(),
    UserService:asClass(UserService).singleton()
})
    .register({
    HomeController:asClass(HomeController.bind(HomeController)).singleton(),
    UserController:asClass(UserController.bind(UserController)).singleton(),
    IdeaController:asClass(IdeaController.bind(IdeaController)).singleton(),
    AuthController:asClass(AuthController.bind(AuthController)).singleton(),
    CommentsController:asClass(CommentsController.bind(CommentsController)).singleton()
})
    .register({
    HomeRoutes:asFunction(HomeRoutes).singleton(),
    CommentRoutes:asFunction(CommentRoutes).singleton(),
    UserRoutes:asFunction(UserRoutes).singleton(),
    AuthRoutes:asFunction(AuthRoutes).singleton(),
    IdeaRoutes:asFunction(IdeaRoutes).singleton()

})
    .register({
        User:asValue(User),
        Idea:asValue(Idea),
        Comment:asValue(Comment)
    })
    .register({
        UserRepository:asClass(UserRepository).singleton(),
        IdeaRepository:asClass(IdeaRepository).singleton(),
        CommentRepository:asClass(CommentRepository).singleton()
    })
    
module.exports=container