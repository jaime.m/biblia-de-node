const BaseReposotory = require('./base.repository')
let _user=null

class UserRepository extends BaseReposotory{
    constructor({User}){
        super(User)
        _user=User
    }

    async getUserByUsername(username2){
       const {username}=username2
        
        
        return await _user.findOne({username})
    }
}

module.exports =UserRepository