class BaseRepository{
    constructor(model){
        this.model=model
    }
    async get(id){
        return await this.model.findById(id)
    }
    async getAll(pageSize=5,pageNumber=0){
        //skip and limit

        console.log(pageSize+'-'+pageNumber)
        let skips= pageSize*pageNumber-1
        if(pageNumber===0)
            skips=0
        console.log(skips)
        return await this.model.find().skip(skips).limit(pageSize)
    }
    async create(entity){
        console.log(entity)
        return await this.model.create(entity)
    }
    async update (id,entity){
        return await this.model.findByIdAndUpdate(id,entity,{new:true})
    }
    async delete (id){
         await this.model.findByIdAndDelete(id)
         return true
    }

}

module.exports = BaseRepository