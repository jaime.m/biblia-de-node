
class BaseService{
    constructor(repository){
        this.repository=repository
    }

    async get(id){
        if(!id){
            const error = new Error()
            error.message='id must be send'
            error.status=400
            throw error
        }
        const currentEntity = await this.repository.get(id)
        if(!currentEntity){
            const error = new Error()
            error.message='Entity does not found'
            error.status=404
            throw error
        }
        return currentEntity
    }

    async getAll(pageSize,pageNum){
        return await this.repository.getAll(pageSize,pageNum)
    }

    async create(entity){
        console.log(entity)
        return await this.repository.create(entity)
    }

    async update(id, entity){
        if(!id){
            const error = new Error()
            error.message='id must be send'
            error.status=400
            throw error
        }

        return await this.repository.update(id,entity)
    }

    async delete(id){
        if(!id){
            const error = new Error()
            error.message='id must be send'
            error.status=400
            throw error
        }
        return await this.repository.delete(id)
    }
}

module.exports= BaseService