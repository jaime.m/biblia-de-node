const express = require('express')
const cors = require('cors')
const helmet =require('helmet')
const compression = require('compression')
const {ErrorMiddleware,NotFoundMiddleeare} =require('../middlewares')
require('express-async-errors')


module.exports= function({HomeRoutes, UserRoutes, IdeaRoutes,CommentRoutes,AuthRoutes}){
    const router =express.Router()
    const apiRoutes= express.Router()
    //Se puede limitar que a la API solo haga peticiones un cliente en específico(cors)
    //Default middlewares
    apiRoutes
    .use(express.json())
    .use(cors())
    .use(helmet())
    .use(compression())

    apiRoutes.use("/home",HomeRoutes)
    apiRoutes.use("/user",UserRoutes)
    apiRoutes.use("/idea",IdeaRoutes)
    apiRoutes.use("/comment",CommentRoutes)
    apiRoutes.use("/auth",AuthRoutes)

    router.use("/V1/api",apiRoutes)

    router.use(NotFoundMiddleeare)

    router.use(ErrorMiddleware)

    return router
}
