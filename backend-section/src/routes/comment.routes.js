const {Router} = require('express')

module.exports = function({
    CommentsController
}){
    const router=Router()
    router.get("/:ideaId",CommentsController.getIdeasComments)
    router.get("/:commentId/unique",CommentsController.get)

    router.post("/:ideaId",CommentsController.createComment)

    router.patch("/:commentId",CommentsController.update)
    router.delete("/:commentId",CommentsController.delete)


    return router
}