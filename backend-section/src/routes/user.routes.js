const {Router} = require('express')
const {AuthMiddleware,PaseIntMiddleware,CacheMiddleware}= require('../middlewares')
const {CACHE_TIME} = require('../helpers')
module.exports = function({
    UserController
}){
    const router=Router()
    router.get("/:userId",UserController.get)
    router.get("/",[AuthMiddleware,PaseIntMiddleware,CacheMiddleware(CACHE_TIME.ONE_HOUR)],UserController.getAll)
    router.patch("/:userId",UserController.update)
    router.delete("/:userId",UserController.delete)

    return router
}