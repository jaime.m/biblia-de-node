const {Router} = require('express')

module.exports = function({
    AuthController
}){
    const router=Router()
    router.post("/signup",AuthController.singup)
    router.post("/signin",AuthController.singin)
    return router
}