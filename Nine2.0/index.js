const express = require('express')
const server = express()
const {PORT} = require('./config')
const { HomeRoutes, QuotesRoures} = require('./routes')
const {NotFoundMiddleware} =require('./middlewares')


server.use(express.static('./public'))
server.use(express.json())
//Body parser

server.use('/', HomeRoutes )
server.use('/', QuotesRoures )

server.use(NotFoundMiddleware)
server.listen(PORT,()=>{
    console.log(`application running on port ${PORT}`);
})

//MVC
 